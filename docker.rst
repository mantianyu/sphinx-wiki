Docker
======

一站式文档
----------

docs.docker.com

架构
----

docker 是 C/S 架构, 一个 docker daemon 运行在后台, 我们直接使用的是 docker 的前端程序. 运行前端程序时, 前端程序会将相关的指令发给 daemon, daemon 解析执行再返回给前端.

镜像与容器
----------

> An image is a filesystem and parameters to use at runtime. It doesn’t have state and never changes. A container is a running instance of an image. When you ran the command, Docker Engine:
> * checked to see if you had the hello-world software image
> * downloaded the image from the Docker Hub (more about the hub later)
> * loaded the image into the container and “ran” it

镜像来源
--------

任何人都可以创建自己的镜像, 并将其放到 Docker Hub 上. docker daemon 默认会知道从 Docker Hub 上找镜像.

镜像的创建
^^^^^^^^^^

自己创建镜像需要一个 Dockerfile 描述文件, 写好 Dockerfile 后执行 docker 命令, docker daemon 就会创建相应的镜像. 再执行 `docker images` 命令就能看到创建的这个镜像.

可以看到创建镜像我们只提供了一个描述文件, 没有提供创建镜像所需要软件和文件等, 也没指定创建好的镜像放在那里, 实际上我们自己创建的镜像和下载的镜像是放在一个专门的目录里的, 我们不需要操心镜像在本地的位置, docker 自己能找到它.

提示
^^^^

docker 镜像一般是尽可能做的小, 以节省带宽和存储. 比如 Docker Hub 中的 ubuntu 镜像, 默认是连 ifconfig, ping 这样的工具都不包含的, 初次之外默认还砍掉了很多其他组件, 所以 ubuntu 的镜像总共才 100MB. 安装以后如果需要的话可以自己 apt-get install 添加.

Docker for Mac 的问题
---------------------

Mac 版的 docker 没有 docker0 接口, 宿主机无法与容器直接通信.

https://docs.docker.com/docker-for-mac/networking/#use-cases-and-workarounds

Docker 的存储理解
-----------------

理解镜像, 容器, 层以及存储驱动的概念:

https://docs.docker.com/engine/userguide/storagedriver/imagesandcontainers/

要持久化存储容器里的改动, 一个是 commit 自己的改动, 构建新自己的新的镜像. 一个是挂载 data volume, 将数据存 data volume 上.

data volume 就是宿主机上的文件或目录, 容器启动时可以挂载这个文件或目录. 多个容器可以共享相同的 data volume.

https://docs.docker.com/engine/userguide/storagedriver/imagesandcontainers/#data-volumes-and-the-storage-driver

Docker 的网络绑定
-----------------

https://docs.docker.com/engine/userguide/networking/default_network/binding/

打造自己的开发环境
------------------

使用 macbook 有半个月了, 很好, 但是在服务器开发环境方面和 linux 确实还是有点距离的, 最简单的 php + fpm + nginx 的环境都不好搞. 所以今天想借助 docker 搞一个这样的环境试一下.

方法一:

所做的修改及时 commit

方法二:

自己写 Dockerfile 或者 compose.yaml

方法三:

使用这个项目: https://github.com/phusion/baseimage-docker
