PHP
===

最佳实践
--------

http://www.phptherightway.com/

PEAR 与 PECL
------------

PEAR 与 PECL 是早期的 PHP 包管理系统, 详细的介绍可以参见这篇文章:

`https://cifer.me/php-pear-pecl <http://cifer.me/php-pear-pecl>`_

Composer
--------

现代化的 PHP 包管理系统

每个包有自己的 composer.json. composer.json 中可以描述自己所依赖的其他包, 以及相应的 autoload 指令以方便别人依赖自己时知道如何添加 autoload 规则.

当要被安装的包还依赖其它包时, 其他的包也会被安装, 并被解析 autoload 字段, 一并加入 vendor/composer/autoload_*.php 这些文件中.

vendor/autoload.php
^^^^^^^^^^^^^^^^^^^

我们只需要 require 项目目录下的 vendor/autoload.php 就行了. composer 所安装到 vendor/ 下的依赖库应当自身不包含 vendor/ 目录, 因为那么做是不规范的.

当然就算某个库不小心把自己的依赖库 vendor/ 目录提交到 pkglist.org 了, 只要这个库的 composer.json 中的 requires 指令写了这些库, 在我们的项目中这些依赖库还是会被安装到我们的项目目录的 vendor/ 目录下, 我们依然只需要 require 我们项目目录的 vendor/autoload.php 就可以. 这个不小心提交了自己的 vendor/ 目录的库, 它的 vendor/ 目录我们肯定是不会执行 require 的, 只有可能是这个库自己在自己的代码里执行了 require.

如果这个库的 composer.json 中的 requires 指令没有全包含所依赖的库, 那也没问题, 这样这个库就相当于是 "自包含依赖" 的, 我们更不需要操心安装它的依赖了, 它自己的代码中自会 rquire 它那层级的 vendor/autoload.php.

类自动加载
----------

__autoload() 废弃.

spl_autoload_register() 推荐.

因为后者更加灵活.

推荐实践
^^^^^^^^

类自动加载的推荐实践在 PSR-0 以及 PSR-4, PSR-0 中的某些内容已经被 PSR-4 取代了. 最明显的一点是在 PSR-0 中要求类名中的下划线 "_" 转换成目录分隔符, 在 PSR-4 中则取消了这一规定.

CodeIgniter
-----------

路由机制, 在添加自定义的路由时, 匹配规则中不要添加末尾的反斜杠, 比如,

要这么写:

  $route['product/(:num)'] = 'catalog/product_lookup_by_id/$1'

而不是这么写:

  $route['product/(:num)/'] = 'catalog/product_lookup_by_id/$1'

因为我们在输网址或者前端在调借口的时候, 可能是不会带上最后的反斜杠的, 所以会匹配不上第二种写法.

启动过程
^^^^^^^^

1. index.php 中定义 ENVIRONMENT, $system_path, $application_folder 等变量, 然后执行 $system_path/core/CodeIgniter.php
2. $system_path/core/CodeIgniter.php 引入 core/Common.php, 其中定义了 load_class(), show_404()  等全局方法. 然后调用 load_class() 引入各个核心类, 包括 core/Controller.php 类.
