阅读
====

博客
----

* 酷壳: http://coolshell.cn
* 风雪之隅: http://www.laruence.com/

书单
----

已读
^^^^

https://book.douban.com/mine?status=collect

在读 && 待读
^^^^^^^^^^^^

技术类
~~~~~~

* 深入理解 Nginx, 第二版(2016), 陶辉

金融类
~~~~~~

* 富爸爸穷爸爸全集 (22 册)
