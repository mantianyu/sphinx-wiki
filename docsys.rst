文档与排版系统
==============

reStructuredText
----------------

reST 是伴随 docutils 项目而生的, 是 docutils 所采用的标记文本格式, 理所当然的, docutils 项目中包含了 reST 文本格式的解析器.

后来 reST 格式流行起来, 很多其他项目或组织也采用这种格式, 它们或者开发自己的解析器 (比如 github markup), 或者直接用 docutils 的解释器, 或者部分依赖 docutils 部分自己改造以及扩展解释器 (比如 sphinx).

快速查阅:

http://docutils.sourceforge.net/docs/user/rst/quickref.html

语法手册:

http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html

指令手册:

http://docutils.sourceforge.net/docs/ref/rst/directives.html

关于 reStructuredText 的一切:

http://docutils.sourceforge.net/rst.html

数学公式支持
^^^^^^^^^^^^

易混淆的地方
^^^^^^^^^^^^

reST 中的一些语法有点容易混淆, 一个是指令, 指令的语法是这样的::

    +-------+-------------------------------+
    | ".. " | directive type "::" directive |
    +-------+ block                         |
            |                               |
            +-------------------------------+

reST 还有个结构是显式标记, 它的语法是这样的::

    +-------+-------------------------------+
    | ".. " | "[" mark specific "]"         |
    +-------+ block                         |
            |                               |
            +-------------------------------

指令是 ".. " 之后必须跟指令类型, 然后是两个冒号 "::", 二显式标记 ".. " 之后是什么就和标记自己有关了, 超链接的标记和指令的可能是最像的:

    .. hyperlink_name: link_block

但是要注意超链接名字后面跟的是一个冒号而不是两个.

sphinx
------

sphinx 依赖了 docutils 项目, 但也不是完全依赖, docutils 起初定义许多 reST 的指令, sphinx 都自己重新实现了. 除此之外, sphinx 还扩展了很多自己的指令 (得益于 reST 方便的指令扩展机制), 这些都在 sphinx 的扩展里, 链接如下:

http://www.sphinx-doc.org/en/stable/extensions.html