网络安全
========

无线网络安全
^^^^^^^^^^^^

无线网络渗透工具
----------------

Airplay-ng
~~~~~~~~~~

updated: 2015-06-27

Attack 0:

Attack 1: 欺骗认证

欺骗认证, 就是指 aireplay-ng 以欺骗的方式通过 WEP 的身份认证 (开放认证或者是共享密钥认证) 并且与 AP 关联. 这个过程并不会产生 ARP 帧. 欺骗认证只能用于 WEP, 不能用于 WPA/WPA2.

aireplay-ng -1 0 -e teddy -a 00:14:6C:7E:40:80 -h 00:09:5B:EC:EE:F2 -y sharedkeyxor ath0
其中:

-1 代表 Attack 类型为欺骗认证
0 代表重新认证 (并关联) 的间隔, 单位是秒, 0 表示不重新认证
-e teddy 指的是 SSID
-a 00:14:6C:7E:40:80 指的是 AP 的 MAC 地址
-h 00:09:5B:EC:EE:F2 指的是你本机的 MAC 地址 (可以是伪造的)
-y sharedkeyxor 是包含了 PRGA xor bits 的文件的名称. 这个选项只在共享密钥认证的时候使用. 对于开放认证, 不需要这个选项
共享密钥认证的机理

http://www.aircrack-ng.org/doku.php?id=shared_key

四步:

Start the wireless interface in monitor mode on the specific AP channel
Start airodump-ng on AP channel with filter for bssid to collect the PRGA xor file
Deauthenticate a connected client
Perform shared key fake authentication
Attack 2: Interactive packet replay

这种攻击方式允许你基于一个捕获了的帧进行重放. 其中捕获的帧可能有两个来源, 一个来源是你处于侦听模式的网卡实时捕获的; 另一个来源是 pcap 文件, 标准的 pcap 格式的文件是 libpcap 库 (tcpdump 项目开发的) 所产生的文件格式, 非常多的软件都使用了 libpcap 这个库, 因此也都支持 pcap 文件格式. 当然, 包括 aircrack-ng 套件. 这使得你可以用别的软件捕获数据帧生成 pcap 文件, 然后用 aircrack-ng 套件读取它.

要理解这种重放攻击方式, 你得了解一点无线数据帧的传输流程.

首先要声明的是: 不是所有捕获的数据帧都能成功重放的, 只有一些特定类型的数据帧才能够被成功的重放. 什么叫做 "成功的重放" 呢? "成功" 意味着, 你重放的数据帧能够被 AP 接受, 并且 AP 会生成一个新的 IV 的数据帧发送出去.

为了达到 "成功" 的目的, 我们要么得捕获到那些不需经过修改, 本来就能够成功用于重放的帧; 要么就是捕获到一些能够经过修改然后成功用于重放的帧. 那么我们先来看看, 不需要修改, 就能够直接捕获过来进行重放的帧具有什么特点.

无需修改, 可直接捕获来用于重放的帧

AP 总是会转发目的地址为广播地址 (FF:FF:FF:FF:FF:FF) 的帧, ARP 请求帧符合这个特点. 此外, 这个帧还必须得是从 Station 到分布式系统的, 也就是帧头的 ToDS 位得是 1 (其实这句话的 意思就是说, 这个帧得是从 Station 到 AP 的, 从 <802.11 权威指南> 60 页会发现, 所有的从 Station 发出去的数据帧, ToDS 位都是 1. 另外, 不要觉得发往分布式系统就是发往有线网络, 分布式系统是包含有线骨干, AP, Station 的集合, 所以仅仅是从 Station 发往 AP, 也可以叫做从 Station 发往分布式系统).

也就是说, 只要我们捕获了的帧是从 Station 发往 AP 的 ARP 请求帧, 我们就可以用它来进行重放了. 那么我们捕获这种帧的选项和参数也就出来了:

-b 00:14:6C:7E:40:80 指定 AP 的 BSSID
-d FF:FF:FF:FF:FF:FF 指定目的地址
-t 1 选出 "ToDS" 位被设为 1 的帧
需要修改后才能用于重放的帧

接下来, 我们来看看那些需要经过修改才能用于重放的帧. 我们的目的不变, 就是为了让 AP 能够生成一个新的 IV, 并转发我们重放的帧. 要选出这样的帧, 我们只需要下面两个条件:

-b 00:14:6C:7E:40:80 指定 AP 的 BSSID
-t 1 选出 "ToDS" 位被设为 1 的帧
在这种情况下, 无所谓目的 MAC 地址是什么, 因为我们会修改目的地址, 对于一个已捕获了的这样的帧, 我们可以对其应用如下两个选项来将其变成可重放的帧:

-p 0841 -p 这个选项用来设置所捕获帧的 "Frame Control" 字段, 0841 是 16 进制值. -p 0841 的结果就是 ToDS 字段被设为 1 (也就成了一个从 Station 发往分布式系统的帧)
-c FF:FF:FF:FF:FF:FF 这个设置设置的是帧的目的地址, 目的地址设为广播地址后, AP 便会产生新的 IV 转发这个帧了. 一转发我们也就能捕获到, 从而我们也就捕获到了这个新 IV.
具体的例子见: http://www.aircrack-ng.org/doku.php?id=interactive_packet_replay#usage

Attack 3: ARP 重放工击

当你指定了一个已关联的 Station 的 MAC, 启动 aireplay-ng 之后, 你可能要等一段时间才会发先攻击开始, 因为这个过程需要等待你指定的那个 Station 发送 ARP 请求包.

BackTrack
~~~~~~~~~

updated: 2015-06-13

国外著名的安全组织 Offensive Security 基于 Ubuntu 开发的一套系统, 内置了很多的渗透, 破解工具

Kali linux
~~~~~~~~~~

updated: 2015-06-13

也是 Offensive Security 组织开发的, 不过是基于 Debian 的, 目的是取代 BackTrack, 至于为什么要开发 Kaili 取代 BackTrack, 可以看这里: https://www.kali.org/news/birth-of-kali/

简而言之, 就是在 Offensive Security 这帮人想要开发 BackTrack 6 的时候, 他们列了一串 "特性列表", 想着在下个版本就要支持这些特性. 结果他们经过研究发现, 要实现这些特性得从头重新开发一版 BackTrack, 底层的操作系统平台也得变.

最终他们决定使用 Debian 替代 Ubuntu, 原来的经典的 /pentest 也因为要遵守 FHS 被去掉了, 这次改动他们做出了很大的努力, 为了对得起自己的这份努力, 也为了体现出这个新版确实与以前的 BackTrack 不同, 他们于是将 BackTrack 改名为 Kali.

NetHunter
~~~~~~~~~

updated: 2015-06-13

同样是 Offensive Security 出的, 基于 Kali Linux 开发的, 专用于移动设备的 Android 系统.

它与 Kali Linux 的区别就是: Kali Linux 是一个标准的 Linux 操作系统, 像 Linux 一样能够被安装在很多的设备上; 而 NetHunter 则是基于 Kali Linux, 并且被深度调整, 定制为运行在 ARM 平台 (多为智能手机, 平台电脑) 上的, 比如 NetHunter 添加了 HID 支持, 者在 Kali Linux 上是不容易实现的.
