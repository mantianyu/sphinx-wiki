P2P 网络
========

eDonkey 网络
------------

(注: eDonkey 并不是一种纯 P2P 的网络)

eDonkey 网络 (也被称作 eDonkey2000 网络 (兴于 2000 年), 或者叫 eD2k), 是一种半中心化的, 用于点对点的分享大文件的网络. eDonkey 网络中会有一些服务器端, 用于索引和定位资源, 但是资源的传输是直接在客户端之间进行的.

维护
^^^^

eD2k 网络以前是由 MetaMachine 公司 (eD2k 的创始者) 维护, 现在这家公司已经不维护它了. eD2k 网络现在由相关的开发者组成的社区维护.

服务器软件
^^^^^^^^^^

有两个, 一个是 MetaMachine 公司开发的免费的闭源软件, C++ 写成, 随着 MetaMachine 不再维护 eD2k 网络, 这个服务器软件也不再维护了; 另一个是个人开发者从头开发的, 纯 C 写成的, 但也是免费闭源的, 叫做 eserver, 如今基本所有的 eD2k 服务器都跑这个软件.

客户端软件
^^^^^^^^^^

很多, 最早的是由 MetaMachine 开发的 eDonkey2000, 依然是闭源免费, 依然是早已经停止维护. 之后是 eMule, 免费开源, 遵守 GPL, 但是只有 windows 版.

eD2k 协议后来加了很多扩展功能, 比如 Kademlia, 这些扩展功能都是社区添加的, 基本都是 eserver 和 eMule 添加的, 和 MetaMachine 没有关系. eD2k 协议一直没有正式的文档, 可以说, eD2k 协议就是 eserver 和 eMule, eMule 和 eMule, eserver 和 eserver 之间的交流.

eD2k 客户端从服务器那里取得资源的地址, 而每一台运行服务器软件的主机都可以成为服务器, 所以 eD2k 中的服务器是动态变化的, 客户端软件可能需要经常更新他们的服务器列表.

资源标识
^^^^^^^^

eDonkey 网络中的每一个文件, 都以一个唯一的 MD4 摘要值来标识, 而不是以文件的名字. 而且, 每一个文件都会以 9500 KB 为单位分成很多小块, 每一块单独计算 MD4 摘要. 这么做有很多好处, 想必你能想得到, 不再详述.

ed2k URI
^^^^^^^^

ed2k 链接用来标识 eDonkey 网络中的文件, 就想 http/https 连接用来标识 www 网络中的文件一样.

ed2k 链接格式像下面这样:

基本的::

  ed2k://|file|<file name>|<file size>|<file hash>|/

包含 hash set 的::

  ed2k://|file|<file name>|<file size>|<file hash>|p=<hash set>|/

包含源地址的::

  ed2k://|file|<file name>|<file size>|<file hash >|/|sources,<IP:port>|/

发展
^^^^

2004 年, eDonkey 网络超过了 FastTrack, 成为世界第一大文件共享网络.

2007 年, BitTorrent 超过了 eDonkey, 成为世界第一的文件共享网络. eDonkey 成为第二.

继任的协议
^^^^^^^^^^

虽然 eDonkey 网络不是一种中心化的网络, 但是 eDonkey 网络对服务器的依赖依然很大, 索引和搜索工作都要在服务器上进行, 服务器在 eDonkey 网络中要遭受很大的流量, 而且很容易被攻击.

为此 MetaMachine 开发了 Overnet, 它实现了 Kademlia 协议, 这是一种分布式哈希表协议. 在 Overnet 网络中, 服务器的负荷能降低 60% 以上. eMule 项目也开发了自己的实现了 Kademlia 协议的 Kad 网络. 另外, eMule 还开发了 source-exchange 特性, 这个特性很好, 具体怎么好可以 Google 之.

MLDonkey 这个客户端软件没有 source-exchange 特性.

客户端软件

* eDonkey2000

  MetaMachine 官方的, 最早的

* eMule

* MLDonkey

澄清
^^^^

eMule 翻译过来是 "电骡", eDonkey 翻译过来是 "电驴", 国内的 VeryCD 啥都不是, 只是盗用了 eMule 和 eDonkey 的名声.

后来出来了一些什么 xMule, aMule 等， 都可以看成是 eMule 的衍生。

参考
^^^^

* 介绍了 ed2k 的链接格式: http://www.emule-project.net/home/perl/help.cgi?l=1&rm=show_topic&topic_id=422
* http://en.wikipedia.org/wiki/EDonkey_network
* http://en.wikipedia.org/wiki/Comparison_of_eDonkey_software
* 正宗的 eMule 的主页, 天朝有很多它的山寨版: http://www.emule-project.net/

BitTorrent
----------

基础
^^^^

BitTorrent 是一种 P2P 协议, 由 Bram Cohen 开发.
BitTorrent 协议中, 一个资源被切分成很多小的部分, 每一个部分称作一个 piece, 所有的与某一资源相关的节点主机 (peer) 构成一个流 (torrent), torrent file 记录着与某个资源相关的所有的 peers.
当某一个不属于这个 torrent 的主机想要下载某个资源时, 这个主机会去某个提供 torrent file 服务的网站下载参考 torrent file, 进而向属于这个 torrent 的 peers 那里要 pieces. 也就是说, 同一个资源的不同 pieces, 可能也会从不同的 peers 那里获得.

当这个主机取得了某一个 piece 之后, 他就会成为这个 piece 的 seed, 同时这个主机也会被加入到这个 torrent file 中并回传到服务商网站. 这样别人再取得这个 torrent file 时, 就会发现已经包含了这台新加入的主机 (seed).

为了让每个人都能够定位其他人, 就需要有一个中心化的服务器, 他知道所有 torrent 的所有 peers 的位置. 这样的角色叫做 tracker. 他通常也是提供 torrent file 的仓库.

海盗湾目前是世界上最大的 BitTorrent Tracker, 而且, 永远封不掉.

Hashing
^^^^^^^

与 ed2k 网络一样, 在 BT 网络中的所有 pieces, 都由一个唯一的 hash 值标识, BT 使用 SHA-1 算法.

分享流程
^^^^^^^^

当 peer 要分享一个文件时, 他首先会创建一个 torrent file, 这个 torrent file 会包含者要分享的文件的元信息, 以及 tracker 的信息. 制作完了 torrent file 之后, 可以发不到 tracker 上, 也可以不通过 tracker 直接传给其它的 peer. 其它的 peers 想要下载这个文件的话, 就要读取这个 torrent file, 取得 tracker 的地址, 进而取得 seeds 的地址.

关于分享流程, 可以看维基的这个页面: http://en.wikipedia.org/wiki/BitTorrent#Operation, 讲得很好.

问题
^^^^

家用的路由器可能需要开启 NAT 映射, 因为其他的 peer 可能会主动来连接你的主机.

索引 torrent
^^^^^^^^^^^^

BT 协议里没有规定如何存储 torrent file, 这个 index site 或者 tracker 的事.

客户端软件
^^^^^^^^^^

- BitTorrent

  官方的, 最早的

- uTorrent
- Transmission
- BitComet

参考
^^^^

- http://www.bittorrent.com/help/manual
- BitTorrent 软件的功能: http://www.bittorrent.com/help/manual/chapter0102
- http://en.wikipedia.org/wiki/BitTorrent
- 详细讲解了 BT 分享过成: http://en.wikipedia.org/wiki/BitTorrent#Operation

磁力链接
--------

磁力链接, 是一种标识 P2P 网络中的资源的链接格式, 这种链接格式与 ed2k, bt 的不同在于, 链接中不包含资源的位置信息, 而是包含资源的内容信息 --- 确切来讲, 是资源的 hash 值.

因为磁力链接标识资源是基于内容 (或者说是 metadata), 而不是位置, 所以磁力链接可以被认为是一种 统一资源名称 (Uniform Resource Name), 而不是 统一资源定位符 (Uniform Resource Locators).

详解
^^^^

磁力链接基于资源的内容指向某个资源, 就好像书本的 ISBN 号码一样 --- ISBN 号码并不记录书所在的地理位置. 不过与 ISBN 不同的是, 磁力链接中的 hash 值, 任何拥有资源的人都可以生成, 不需要有一个中央机构. 也就是说, 任何人都可以发起一个磁力链接.

相比 BitTorrent, 磁力链接还有一个好处, 就是它的便携性, 它只是一行纯文本, 可以方便的被复制粘贴到即时通讯工具, 或者电子邮件中. 但是 BT 中的 torrent file 就不具备这种优点.

格式
^^^^

磁力链接的格式很像 HTTP 协议 URL 的查询字符串. 在磁力链接中最常用的参数是 "xt" (exact topic), 这个参数的值是 URN 格式的 [1], 表示资源的 hash 值, 像下面这样:

> magnet:?xt=urn:sha1:YNCKHTQCWBTRNJIV4WNAE52SJUQCZO5C

这个代表了经 Base32 算法加密过的某个资源的 sha1 hash 值. 注意, hash 值只是指定了某个确定的资源, 没有指定位置, 客户端需要做具体的搜索来确定资源的位置.

在磁力链接中还有其他的参数, 如下:

- dn (display name), 表示显示给用户的名字
- kt (keyword topic), 可以用来搜索资源
- 客户端定制的参数, 必须以 "x." 开头

不同的参数以 "&" 隔开, 和 URL 的 query string 一样.

xt 参数中所用到的名字空间
^^^^^^^^^^^^^^^^^^^^^^^^^

TTH (Tiger Tree Hash)
^^^^^^^^^^^^^^^^^^^^^

  xt=urn:tree:tiger:[ TTH Hash (Base32) ]


这种 hash 算法是 Direct Connect 和 Gnutella 2 网络中所使用的, 客户端会从这两个网络里搜索给定 hash 值的资源.

ED2K
^^^^

  xt=urn:ed2k:[ ED2K Hash (Hex) ]


这个自然是用于 ED2K 网络.

Kazaa Hash
^^^^^^^^^^

  xt=urn:kzhash:[ Kazaa Hash (Hex) ]

用于 FastTrack 网络.

BTIH (BitTorrent Info Hash)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

  xt=urn:btih:[ BitTorrent Info Hash (Hex) ]

用于 BT 网络.

历史与未来
^^^^^^^^^^

在历史的年轮中, 各大提供 P2P 服务的网站必然都要面临版权的问题, 而且几乎所有的这种网站最终都会因法律问题被关闭或者被封杀. eDonkey 早已死去, 接班人 BT 也已经苟延残喘, BT 的接班人就是磁力链接, 磁力链接也被称为 BT 2.0!

支持
^^^^

绝大多数的客户端都支持磁力链接, 包括 BT 客户端和 ED2K 客户端, 如: uTorrent, eMule, BitComet, BitTorrent, MLDonkey 等等.

海盗湾网站从 2012 年开始慢慢的提供磁力链接, 改用磁力链接之后带来的好处是存储空间可以非常小. 假如海盗湾将其站点上的资源全部改为磁力链接的话, 那么海盗湾全站的数据压缩之后也就有 90M 大小.

脚注
^^^^

- URN 即 Uniform Resource Name.

  注意: 统一资源名称 (Uniform Resource Name) 和 统一资源定位符 (Uniform Resource Locators) 都是属于 统一资源标识符 (Uniform Resource Indentifier).

  - 其中, URL 的格式你们知道的, 参考某一个网址就行了. 而 URN 的格式可能见少见一些, 它的格式是这样的:
    
        <URN> ::= "urn:" <NID> ":" <NSS>

  - 其中 "urn:" 是个固定常量, 而且是大小写敏感的, NID 是名字空间的标识符, 它决定了 NSS 的格式. 大家应该都熟悉的名字空间有 isbn, uuid, ietf 等. 看下面几个例子:

    - urn:isbn:0451450523
    - urn:ISSN:0167-6423
    - urn:ietf:rfc:2648
    - urn:uuid:6e8bc430-9c3a-11d9-9669-0800200c9a66

参考
^^^^

- 英文维基页
- http://www.zhihu.com/question/21734493

> 摘录参考 2:

  ed2k链接可以通过一些简单算法转换成各大下载软件的专用下载链接（比如迅雷/快车/旋风/BitComet），当然这些链接也都不是http的, 至于BT种子方面, 直接转换为种子我是没有成功. ed2k链接存储的内容是“文件名”、“文件大小”、“文件的eD2k Hash”, 能特定文件的只有eD2k Hash，这是一种文件校验算法,电驴等客户端通过这个校验码在ED2K和KAD网络中寻找文件本体下载,这是一个不可逆的——你不可能直接从ED2K中算出文件本身, BT种子(torrent)文件中存储的信息更为复杂，可参见维基百科.除了文件名和大小与ED2K中的信息重复,最重要的校验部分，torrent采用的是文件块逐个校验的方式,即存储了许多校验码(sha-1),即使文件本身大小就在文件块大小以下,也无法进行转换, BT下载方式除了种子文件的方式以外还有磁力链接(Magnet)方式磁力链接和ED2K其实是比较类似的, 但是支持多种算法, 其中就有ed2k hash, 那么理论上就能够把ED2K转换成Magnet是可行的? 假如有一个ED2K(考虑到社区规范,链接为自己修改而成并非真实链接)
  
::

    ed2k://|file|Movie1.mkv|3395472522|3724df27812a55f36a5b192859c513c2|/
    
那么根据Magnet的协议应该转换为

    magnet:?xt=urn:ed2k:3395472522|3724df27812a55f36a5b192859c513c2&xl=6395472522
    
然后使用BT客户端解析Magnet链接,获取种子文件，就完成了种子转换工作.

很遗憾，我手头的两个工具µTorrent和迅雷,都无法识别magnet:?xt=urn:ed2k的方式,如果谁有能支持的工具望推荐.

理论上如此——但方法不是只有转换一种,互联网上应该有一些收集ED2K链接和种子文件对应关系的网站,可以找下并试着搜索一下,也许会有一些收获

显然题主的目的应该是想把一个网上看到的ed2k链接离线到百度云中,但是百度云的离线功能目前只支持http和bt种子(连磁力链接都不支持),离线下载支持比较多的建议使用迅雷或者旋风。

我的情况正好和题主相反，我会把下载好的电影看完后转换成ed2k链接存储到百度云上.因为当年百度云空间比较小且有收费可能(dropbox更小且认为存储娱乐性信息百度云足够)，为了保证我的百度云空间不会很快被沾满，我会在电影看完之后将文件转换为ED2K链接。如果我有再次观看需求时,直接下载链接用迅雷秒杀下载即可,不过现在百度云有3T以上空间了,目前我也只会大于4G的文件这样做了。

资源共享网络编年史
------------------

最后更新: 2014 年 08 月 08 日

本文是参考 1 维基页面 "Timeline_of_file_sharing" 的精简版, 只保留了对网络服务的介绍, 这篇文章会不定时更新.

- 1999 年 06 月

  Shawn Fanning 创立 Napster. Napster 是一种中心化, 或者叫做半中心化网络结构, 资源的索引和搜索是在 Napster 的服务器上进行, 但是服务器上不存放或只缓存部分资源文件, 客户端连接服务器查找并定位资源, 然后直接从资源所在的主机下载资源. Napster 主要提供的是音乐下载服务.

  不难想象, Napster 客户端软件会将自己主机上的资源上报给 Napster 服务器, 服务器对资源进行索引.

在计算机科学界, modern 化的文件共享始于 21 世纪, 进入 2000 年之后, 很多文件共享协议以及文件格式被发明出来.

- 2000 年 03 月

  Gnutella 网络被创立, 这是世界上第一个完全去中心化的资源共享网络, 创始人在网络创立之时同时发布了相应的客户端软件. 没有服务器端软件, 因为没有服务器. 十年后, Gnutella 网络有数百万的用户基数了.

- 2000 年 09 月

  Jed McCaleb 发布了 eDonkey网络的客户端 (eDonkey2000) 与服务器端软件, 在资源分享的历史上引入了 hashing 技术, 后来的 Gnutella2 也采用了这个技术, 来检测减少重复的文件. eDonkey 网络是一种半中心化的网络.

  关于 eDonkey 可以参考另一篇文章 "eDonkey 网络".

- 2001 年 02 月

  Napster 在此之前官司不断. 同时再此月 Napster 用户达到巅峰的 2640 万.

- 2001 年 03 月

  Niklas 等人发布 FastTrack P2P 协议, 相应的客户端 Kazaa 也发布出来, 但是 Kazaa 桌面客户端绑定了流氓软件. FastTrack 协议应当也归为半中心化的协议, 需要服务器端.

  同年 04 月, StreamCast 公司取得了 FastTrack 协议的授权书, 发布了 Morpheus 客户端. StreamCast 这家公司之前维护过 Napster 的服务器. Morpheus 客户端后来也支持 Gnutella 网络, 曾是红极一时的客户端软件.

  同年 07 月, Napster 因版权问题被关闭.

- 2001 年 07 月
  Bram Cohen 设计了 BitTorrent 协议, 同时发布了第一款 BitTorrent 客户端, 名字也叫 BitTorrent.
  关于 BitTorrent 可以参考我另一篇文章.

- 2002 年

  05 月, eMule 客户端发布, 很快替代 eDonkey2000 客户端, 成为 eDonkey 网络的首选客户端.
  07 月, eDonkey2000 客户端的作者介绍了 Overnet 网络, 这是一种在 eDonkey 基础上实现了 Kademlia 协议的网络, 而 Kademlia 则是一种分布式哈希表构建协议.
  11 月, Gnutella2 协议被提出, 它也引入了 hashing 技术, 以减少文件重复的问题.

- 2003 年

  01 月, isoHunt 网站建立, 这是一个 torrent 文件的索引站. torrent 文件是 BitTorrent 协议中的.
  04 月, Demonoid 站建立, 也是一个 torrent 索引站.
  11 月, The Pirate Bay (海盗湾) 创立.
  12 月, eMule 支持 Kad 网络, 这也是一个实现了 Kademila 协议的网络

- 2006 年
  02 月, 位于瑞士的, 世界上最大的 eDonkey 网络之一 Razorback2, 被关闭了.
  05 月, 海盗湾也被关闭

- 2007 年
  08 月, 海盗湾重新开张, 使用 suprnova.org 域名
  11 月, Demonoid 网站被关闭.

参考
^^^^

- http://en.wikipedia.org/wiki/Timeline_of_file_sharing
